# CMake generated Testfile for 
# Source directory: /home/nvidia/surround_early_fusion/surround_view_ws/src
# Build directory: /home/nvidia/surround_early_fusion/surround_view_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("cam_activation")
subdirs("mask_rcnn_ros")
subdirs("ouster_example/ouster_client")
subdirs("ouster_example/ouster_viz")
subdirs("overlay")
subdirs("gscam")
subdirs("ouster_example/ouster_ros")
