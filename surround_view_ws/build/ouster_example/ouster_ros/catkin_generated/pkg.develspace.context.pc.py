# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/nvidia/surround_early_fusion/surround_view_ws/devel/include;/home/nvidia/surround_early_fusion/surround_view_ws/src/ouster_example/ouster_ros/include".split(';') if "/home/nvidia/surround_early_fusion/surround_view_ws/devel/include;/home/nvidia/surround_early_fusion/surround_view_ws/src/ouster_example/ouster_ros/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;message_runtime;pcl_ros;std_msgs;sensor_msgs;geometry_msgs;ouster_client;ouster_viz".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-louster_ros".split(';') if "-louster_ros" != "" else []
PROJECT_NAME = "ouster_ros"
PROJECT_SPACE_DIR = "/home/nvidia/surround_early_fusion/surround_view_ws/devel"
PROJECT_VERSION = "0.1.0"
