#!/usr/bin/env python
import numpy as np
import cv2
import ros_numpy




def preprocess_cloud(cloud_from_ros):
	points = ros_numpy.point_cloud2.pointcloud2_to_array(cloud_from_ros)  #
	aux = points.reshape(-1)
	xyz = np.array(aux[['x', 'y', 'z']].tolist())
	xyz.setflags(write=1)
	xyz[:, 0] = -xyz[:, 0]
	xyz[:, 1] = -xyz[:, 1]
	return xyz

def convert_cloud_to_pixel_space(lidar_2_cam_matrix, cam_matrix, lidar_pts):
	cam_pts = np.matmul(lidar_2_cam_matrix[:3, :], np.pad(lidar_pts.T, ((0, 1), (0, 0)), 'constant', constant_values=1))
	plane_pts = np.matmul(cam_matrix, cam_pts)
	q = plane_pts[:2, :] / plane_pts[2, :]
	q = q.T.astype(int)
	return q

def get_lidar_mask_overlay(lidar_pixels, mask_img):
	# return indexes
	width = mask_img.shape[1]
	height = mask_img.shape[0]
	def get_value_mask(i, j):
		if i < height and i > 0 and j < width and j > 0:
			mask_value = mask_img[i, j]
		else:
			mask_value = 0
		return mask_value
	indexes = np.where(np.vectorize(get_value_mask)(lidar_pixels[:, 0], lidar_pixels[:, 1]) != 0)
	return indexes

def show_image_overlay(original_image, label_list=None, values_list=None, bbox_list=None, metrics_name='distance'):
	for idx, label in enumerate(label_list):
		cv2.putText(original_image, str(label) + ' at '+ metrics_name + ' : '+str(values_list[idx]),
					(bbox_list[idx][0], bbox_list[idx][1]+10),
					cv2.FONT_HERSHEY_SIMPLEX, 0.9, (0,255,0), 2)
		# cv2.rectangle(cv_image_n, start_point, end_point, (255, 0, 0), 2)
	return original_image

def get_cloud_color_list(pixel_list, color_img):
	pass
	# colors = list()
	# for i, pt in enumerate(pixel_list):
	# 		if 0 < pt[0] < color_img.shape[1] and 0 < pt[1] < color_img.shape[0]:
	# 			color = org_image[int(pt[1]), int(pt[0])]
	# 	colors[i] = color
	# 	pass

def return_minimum_distance_from_origin(cloud):
	dists = np.linalg.norm(cloud, axis=1)
	min_dist = np.amin(dists)
	return min_dist