#!/usr/bin/env python

import math
import rospy
import sys
import cv2
import cv_bridge
from image_geometry import PinholeCameraModel
import tf
from sensor_msgs.msg import Image, CameraInfo, PointCloud2
from mask_rcnn_ros.msg import Result as MRCNNResult
import message_filters
from overlay_utils import (preprocess_cloud, convert_cloud_to_pixel_space, get_lidar_mask_overlay, \
	return_minimum_distance_from_origin, show_image_overlay, get_cloud_color_list)
from calibration import lidar_2_cam_north, camera_matrix_north

bridge = cv_bridge.CvBridge()
image_overlay_flag = True
filter_cloud_flag = False
color_filter_cloud_flag = True

def process_fusion(image, seg, cloud):
	print('Debugging : ', image.header, seg.header, cloud.header)

	#transform clud to image frame

	pts = preprocess_cloud(cloud)
	pixels_from_lidar = convert_cloud_to_pixel_space(lidar_2_cam_north, camera_matrix_north, pts)
	org_image = None

	labels = seg.class_names
	masks = seg.masks
	indexes_groups = list()
	bboxes = [(box.x_offset, box.y_offset) for box in seg.boxes]
	values = list()

	for idx, label in enumerate(labels):
		mask_img = bridge.imgmsg_to_cv2(masks[idx], 'mono8')
		indexes = get_lidar_mask_overlay(pixels_from_lidar, mask_img)
		dist = return_minimum_distance_from_origin(pts[indexes])
		indexes_groups.append(indexes)
		values.append(dist)

	print('labels', labels)
	print('distances', values)

	if image_overlay_flag:
		org_image = bridge.imgmsg_to_cv2(image, 'bgr8')
		overlay_img = show_image_overlay(org_image, label_list=labels, values_list=values, bbox_list=bboxes)
		out_overlay = bridge.cv2_to_imgmsg(overlay_img, 'bgr8')
		overlay_pub.publish(out_overlay)

	if filter_cloud_flag:
		if org_image is None:
			org_image = bridge.imgmsg_to_cv2(image, 'bgr8')
		for index_group in indexes_groups:
			colors = get_cloud_color_list(pixels_from_lidar[index_group], org_image)
		send_cloud(pts[index_group], colors)


def send_cloud(data, colors):
	pass
# TODO review this pipeline
	# global test_pointcloud_publisher, frame_id
	# #print('debuyg colors  ',colors)
	# dtype = [
	# 	('x', np.float32),
	# 	('y', np.float32),
	# 	('z', np.float32),
	# 	('r', np.float32),
	# 	('g', np.float32),
	# 	('b', np.float32)
	# ]
	# cloud_arr = np.array([(i[0][0],i[0][1], i[0][2],i[1][2]/255.0,i[1][1]/255.0, i[1][0]/255.0) for i in  zip(data, colors)], dtype=dtype)
	# cloud_msg = ros_numpy.point_cloud2.array_to_pointcloud2(cloud_arr, frame_id='os1_sensor')
	# test_pointcloud_publisher.publish(cloud_msg)









def receive_image(img):
	print('Image header : ', img.header)

def receive_cloud(cld):
	print('Cloud header : ', cld.header)

def receive_segmentation(seg):
	print('Segmentation header : ', seg.header)




if __name__ == '__main__':
	try:

		# Initialize the node and name it.
		rospy.init_node('lidar_overlay_image')
		rate = rospy.Rate(10.0)

		image_name = rospy.resolve_name('/cn/camera/image_raw')
		print('Waiting for input image from ' + image_name)

		cloud_name = rospy.resolve_name('/os1_cloud_node/points')
		# print('Waiting for point cloud data from ' + cloud_name)
		
		segment_name = rospy.resolve_name('/mask_rcnn/result/')
		print('Waiting for instance segmentation from ' + segment_name)

		mrcnn_sub = message_filters.Subscriber(segment_name, MRCNNResult)
		mrcnn_sub.registerCallback(receive_segmentation)

		camera_sub = message_filters.Subscriber(image_name, Image)
		# camera_sub.registerCallback(receive_image)

		cloud_sub = message_filters.Subscriber(cloud_name, PointCloud2)
		cloud_sub.registerCallback(receive_cloud)

		ts = message_filters.ApproximateTimeSynchronizer([camera_sub, mrcnn_sub, cloud_sub], 100, 0.1, allow_headerless=True)

		ts.registerCallback(process_fusion)

		overlay_name = rospy.resolve_name('image_overlay')
		filter_cloud_name = rospy.resolve_name('filtered_cloud')
		
		# Publish the lidar overlay image
		overlay_pub = rospy.Publisher(overlay_name, Image, queue_size = 1)
		filter_cloud_pub =  rospy.Publisher(filter_cloud_name, PointCloud2, queue_size = 1)
		print("its publishing")
		#test = transform.lookupTransform('os1_lidar', 'os1_sensor',rospy.Time(0))
		rospy.spin()

	except rospy.ROSInterruptException: pass
