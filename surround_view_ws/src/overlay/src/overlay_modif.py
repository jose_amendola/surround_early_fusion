#!/usr/bin/env python

import math
import rospy
import sys
import cv2
import cv_bridge
from image_geometry import PinholeCameraModel
import tf
from sensor_msgs.msg import Image, CameraInfo, PointCloud2
import struct
import numpy as np
import numpy.lib.recfunctions as rf
from tf.transformations import euler_from_matrix
from tf2_sensor_msgs.tf2_sensor_msgs import do_transform_cloud
from tf2_ros import TransformListener, Buffer
import ros_numpy
from vision_msgs.msg import VisionInfo, Detection2DArray, Detection2D,ObjectHypothesisWithPose

camera ={}
imageOverlay = {}
cameraInfo = CameraInfo()
cameraModel = PinholeCameraModel()
bridge = cv_bridge.CvBridge()

test_pointcloud_publisher =  None
frame_id = None
lidar_data = None

#tf_buffer = Buffer()
#transform = TransformListener(tf_buffer)


velodyneData = []
qw= False

cv_image_n={}
cv_image_s={}
cv_image_e={}
cv_image_w={}


#read camera_info (intrinsic matrix of camera)update with correct intrinsic  matrix. 
def camera_callback(data):
	global cameraModel, camera, frame_id, lidar_data

	cameraInfo = data
	cameraModel.fromCameraInfo( cameraInfo )
	# print(cameraInfo)

#read pointcloud2 data and convert it to a numpy array
def velodyne_callback(data): 
	global velodyneData, qw, test_pointcloud_publisher, frame_id
	formatString = 'ffff'
	if data.is_bigendian:
		formatString = '>' + formatString
	else:
		formatString = '<' + formatString

	p = []

	points = ros_numpy.point_cloud2.pointcloud2_to_array(data) #each point in the matrix is a 10 dimensional vector. (x,y,z,0.0,intensity,reflectivity,ring,noise,t,range)
	# print('debugging frame id',data.header.frame_id)
	frame_id = data.header.frame_id
	velodyneData = points
	qw=True
	#print('debugging ptcloud msg', data.fields)
	#test_pointcloud_publisher.publish(data)


#read bounding box coordinates 
def coord_callback_north(data):
	global coord_north
	print('debug callback detection')
	coord_north=data


def send_ptcloud(data, colors):
	global test_pointcloud_publisher, frame_id
	#print('debuyg colors  ',colors) 
	dtype = [
		('x', np.float32),
		('y', np.float32),
		('z', np.float32),
		('r', np.float32),
		('g', np.float32),
		('b', np.float32)
	]
	cloud_arr = np.array([(i[0][0],i[0][1], i[0][2],i[1][2]/255.0,i[1][1]/255.0, i[1][0]/255.0) for i in  zip(data, colors)], dtype=dtype)
	cloud_msg = ros_numpy.point_cloud2.array_to_pointcloud2(cloud_arr, frame_id='os1_sensor')
	test_pointcloud_publisher.publish(cloud_msg)



# north camera
def image_callback(data):
	global velodyneData, bridge, cv_image_n, test_pointcloud_publisher, frame_id, lidar_data
	print('In input image callback - received rectified image')

	try:
		print('ros image image', data.width, data.height)
		cv_image_n = bridge.imgmsg_to_cv2(data, 'bgr8')
		# cv_image_n = cv2.resize(cv_image_n, (640,480), interpolation = cv2.INTER_AREA)
	except cv_bridge.CvBridgeError as e:
			print('Failed to convert image', e)
			return

	lidar_2_cam = np.array([[ 0.99459605, -0.10373795, -0.00413877,  0.10740025],
			 [-0.00525396, -0.01047908, -0.99993129, -0.04607853],
			 [ 0.10368745,  0.99454946, -0.01096748,  0.0440092 ],
			 [ 0.,          0. ,         0.,          1. ,       ]],dtype=np.float64)

	#trans=np.array([[0.16968111],[-0.19596945],[1.00284876]],dtype=np.float64)# north best
	#rot=np.array([[2.31360781],[-0.10586986],[-2.04029633]],dtype=np.float64)#north best
	rot = lidar_2_cam[:3,:3]
	trans =  lidar_2_cam[:3,3]
	height, width = cv_image_n.shape[:2]
	print('opencv image', width, height)
	#size=[1280,720]
	#focal_length = size[1]
	#center = (size[1]/2, size[0]/2)
	scale_x = 1280/640
	scale_y = 720/480
	camera_matrix = np.array([[scale_x*387.9227328583913, 0.0, scale_x*325.29620322015387], [0.0, scale_y*389.6460825910029, scale_y*234.07029772200596], [0.0, 0.0, 1.0]], dtype = np.float64)
	# camera_matrix = np.array([[387.9227328583913, 0.0, 325.29620322015387],
	# 						  [0.0, 389.6460825910029, 234.07029772200596], [0.0, 0.0, 1.0]],
	# 						 dtype=np.float64)
	# camera_matrix = np.array([[0.3879227328583913, 0.0, 0.32529620322015387], [0.0, 0.3896460825910029, 0.23407029772200596], [0.0, 0.0, 1.0]], dtype = np.float64)

	dist_coeffs = np.array([[-0.27359617796489377, 0.11996859873933836, 0.0009200903124645048, -0.00010554517240543417, -0.03049516837648034]], dtype = np.float64
                         )	
	if qw==True:
		#xyz = ros_numpy.point_cloud2.get_xyz_points(velodyneData)
		aux = velodyneData.reshape(-1)
		xyz = np.array(aux[['x', 'y', 'z']].tolist())
		xyz.setflags(write=1)
		#TODO check coordinate transform
		#xyz[:,[0,1,2]]=xyz[:,[1,2,0]]
		xyz[:,0] = -xyz[:,0]
		xyz[:,1] = -xyz[:,1]
		#print("xyz : ",xyz)
		#ones_=np.ones((xyz.shape[0]))
		#point=np.array([xyz[:,1],xyz[:,2],xyz[:,0]],dtype=np.float64)

		# h, w = cv_image_n.shape[:2]
		# newmatrix, roi = cv2.getOptimalNewCameraMatrix(camera_matrix, dist_coeffs, (w, h), 1, (w, h))
		# cv_image_n = cv2.undistort(cv_image_n, camera_matrix, dist_coeffs, None, newmatrix)
		
		cam_pts = np.matmul(lidar_2_cam[:3,:], np.pad(xyz.T,((0,1),(0,0)),'constant', constant_values=1))
		plane_pts = np.matmul(camera_matrix, cam_pts)
		#print("plane pts : ",cam_pts, cam_pts.shape)
		q = plane_pts[:2,:]/plane_pts[2,:]
		q = q.T
		# (q,_)=cv2.projectPoints(cam_pts.T, np.eye(3) ,np.zeros(3) , camera_matrix, dist_coeffs)
		# (q,_)=cv2.projectPoints(xyz,rot ,trans , camera_matrix, dist_coeffs)
		# q=np.squeeze(q,axis=1)
		print("obj det")
		det_=Detection2DArray()	
		det=Detection2D()
		obj_=ObjectHypothesisWithPose()
		det_.detections=coord_north.detections
		oo={62:"Chair",65:"Bed"}
		for i in range(len(det_.detections)):
			det=det_.detections[i]
			obj_=det.results
			obj=obj_[i]			
			#For some reason the yolo node is sending top left coords to the size fields and bottom right to the center fields
			start_point=(int(det.bbox.size_x),int(det.bbox.size_y))
			# start_point=(1200,0)
			end_point=(int(det.bbox.center.x),int(det.bbox.center.y))
			# end_point=(1280,720)
			
			c=q[:,0]
			d=q[:,1]
			# dist=-1
			re=np.where((q[:,0]>start_point[0])&(q[:,1]>start_point[1])&(q[:,0]<end_point[0])&(q[:,1]<end_point[1])&(cam_pts.T[:,2]>=0.0))
			# re = np.where((cam_pts.T[:,2]>0.03)&(np.arctan(np.abs(cam_pts.T[:,2])/np.abs(cam_pts.T[:,0]))< np.pi/5) )
			# re = np.where((cam_pts.T[:, 2] > 0.0))

			print('debug re ', re[0].shape)
			if re[0].size != 0:
				
				# im_center = np.squeeze(np.mean(q[re], axis=0))
				#print('im center', im_center)
				
				avg=xyz[re,:]
				avg=np.squeeze(avg)
				print('projection q shape', q.shape)
				print('projection avg shape', avg.shape)
				#print('velodyne_data',velodyneData.reshape(-1).shape)
				#send_ptcloud(velodyneData.reshape(-1)[re])
				
				dists = np.linalg.norm(avg, axis=1)
				ref_index = np.argmin(dists)
				colors = list()	
				for i,pt in enumerate(np.squeeze(q[re])):
					color = np.array([0.0,255.0,0.0])
					if 0 < pt[0] < cv_image_n.shape[0] and  0 < pt[1] < cv_image_n.shape[1]:
						color = cv_image_n[int(pt[0]), int(pt[1])]
						#print('debug color')
				# 		# color = np.array([255.0,0.0,0.0])
					if i == ref_index:
						color = np.array([0.0, 0.0, 255.0])
						cv2.circle(cv_image_n, (int(pt[0]), int(pt[1])),1,color.tolist(),3)
					colors.append(color)
				# cv2.circle(cv_image_n, (640,480), 10, (0,0,255.0), 10)
					# if np.all(pt>0, axis=0):
					# print('debug color ', color)
				send_ptcloud(avg[:,:3], colors)
			dist = np.linalg.norm(avg[ref_index,:])
			# dist=abs(np.average(avg,axis=0)[0])
			cv2.rectangle(cv_image_n, start_point, end_point, (255, 0, 0),2)
			cv2.putText(cv_image_n, str(obj.id) + " at : "+str(dist), (start_point[0], start_point[1]+10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (0,255,0), 2)
			
	try:
		print("O")
		
		imageOverlay.publish(bridge.cv2_to_imgmsg( cv_image_n, 'bgr8' ) )
	except cv_bridge.CvBridgeError as e:
		print( 'Failed to convert image', e )
		return


if __name__ == '__main__':
	try:

		# Initialize the node and name it.
		rospy.init_node('lidar_overlay_image')
		rate = rospy.Rate(10.0)

		# look up camera name, after remapping 
		cameraName = rospy.resolve_name( '/cw/camera/camera_info' )
		print('Waiting for camera_info from ' + cameraName)

		# look up image_rect_color 
		imageRectName = rospy.resolve_name( '/cn/camera/image_raw' )
		print('Waiting for input image from ' + imageRectName)

		# look up velodyne data
		velodynePointName = rospy.resolve_name('/os1_cloud_node/points')
		print('Waiting for velodyne point data from ' + velodynePointName)
		
		cordname_north=rospy.resolve_name('/det_n/detections')

		# Subscribe to topic, cameraInfo and callback function.
		camera = rospy.Subscriber( cameraName, CameraInfo, callback = camera_callback)

#choose the camera
		imageRect = rospy.Subscriber(imageRectName, Image, callback = image_callback) # north

		velodynePoint = rospy.Subscriber(velodynePointName, PointCloud2, callback = velodyne_callback)
		
		#choose yolo instance
		coord_north=rospy.Subscriber(cordname_north,Detection2DArray,callback=coord_callback_north)

		imageOverlayName = rospy.resolve_name( 'image_overlay')
		test_pointcloud_name = rospy.resolve_name( 'test_pointcloud')
		
		# Publish the lidar overlay image
		imageOverlay = rospy.Publisher( imageOverlayName, Image, queue_size = 1)
		test_pointcloud_publisher =  rospy.Publisher( test_pointcloud_name, PointCloud2, queue_size = 1)
		print("its publishing")
		#test = transform.lookupTransform('os1_lidar', 'os1_sensor',rospy.Time(0))
		rospy.spin()

	except rospy.ROSInterruptException: pass
