import os 
os.environ["OPENBLAS_CORETYPE"] = "nehalem"
import cv2
import numpy as np
import json
from os1 import OS1
from multiprocessing import Process, Queue, Manager, Array
from os1.packet import (
	AZIMUTH_BLOCK_COUNT,
	CHANNEL_BLOCK_COUNT,
	azimuth_angle,
	azimuth_block,
	azimuth_measurement_id,
	azimuth_timestamp,
	azimuth_valid,
	channel_block,
	channel_range,
	unpack,
	PACKET
)
from os1.utils import xyz_points, build_trig_table, frame_handler
import pcl
import pcl.pcl_visualization
from time import sleep
import math
import select
import struct


visual = pcl.pcl_visualization.CloudViewing()

_unpack = struct.Struct("<I").unpack
def peek_encoder_count(packet):
    return _unpack(packet[12:16])[0]

# The OS-16 will still contain 64 channels in the packet, but only
# every 4th channel starting at the 2nd will contain data .
OS_16_CHANNELS = (2, 6, 10, 14, 18, 22, 26, 30, 34, 38, 42, 46, 50, 54, 58, 62)
OS_64_CHANNELS = tuple(i for i in range(CHANNEL_BLOCK_COUNT))
_trig_table = None


unprocessed_packets = Queue()
extracted_frames = Queue()
point_cloud = None

#visual_plane = pcl.pcl_visualization.CloudViewing()
_display = True
coords = np.empty(shape=(0,3),dtype=np.float32)
_mem_frame = None

_frame_cloud = None

OS1_IP = '169.254.123.132'
HOST_IP = '169.254.17.164'

def handler(packet):
    unprocessed_packets.put(packet)

def spawn_workers(n, worker, *args, **kwargs):
    processes = []
    for i in range(n):
        process = Process(
            target=worker,
            args=args,
            kwargs=kwargs
        )
        process.start()
        processes.append(process)
    return processes

def worker(lidar_ip, host_ip, out_queue) :
	global _trig_table
	os1 = OS1(lidar_ip, host_ip, mode='1024x10')	
	beam_intrinsics = json.loads(os1.get_beam_intrinsics())
	out_queue.put(beam_intrinsics)
	#handler = frame_handler(out_queue)
	buffer = []
	rotation_num = 0
	sentinel = None
	last = None

	def handler(packet):
		nonlocal rotation_num, sentinel, last, buffer, out_queue
		encoder_count = peek_encoder_count(packet)	
		buffer.append(packet)
		if sentinel is None:
			sentinel = encoder_count

		if len(buffer) > 1 and last and encoder_count >= sentinel and last <= sentinel:
			rotation_num += 1
			out_queue.put({"buffer": buffer, "rotation": rotation_num})
			buffer = []
		
		last = encoder_count

	try:
	    os1.run_forever(handler)
	except KeyboardInterrupt:
		for w in workers:
			w.terminate()


def initialize_lidar():
	workers = spawn_workers(1, worker, OS1_IP, HOST_IP, extracted_frames)

	



def find_blobs():
	pass



def start_lidar_pipeline():
	global _frame_cloud 
	initialize_lidar()
	coords = np.empty(shape=(0,3),dtype=np.float32)
	beam_intrinsics = extracted_frames.get()
	beam_alt_angles = beam_intrinsics['beam_altitude_angles']
	beam_az_angles = beam_intrinsics['beam_azimuth_angles']
	_trig_table = build_trig_table(beam_alt_angles, beam_az_angles)
	cam = cv2.VideoCapture(0)
	while True:
		ret_val, img = cam.read(cv2.IMREAD_GRAYSCALE)
		gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		frame_dict = extracted_frames.get()
		while extracted_frames.qsize() >= 1:
			frame_dict = extracted_frames.get()
		#print('rotation ', frame['rotation'])
		coords = np.empty(shape=(0,3),dtype=np.float32)
		size = len(frame_dict['buffer'])
		#print('packets', size)	
		for i,packet in enumerate(frame_dict['buffer']):
			x, y, z = xyz_points(packet)
			frame = np.array(list(zip(x,y,z)), dtype=np.float32)
			coords = np.concatenate((coords, frame), axis=0)

		ptcloud = pcl.PointCloud()
		ptcloud.from_array(coords)
		visual.ShowMonochromeCloud(ptcloud, b'cloud')
		cv2.imshow('blobs', img)
		if cv2.waitKey(1) == 32:
			cv2.waitKey(0)
			filename ='pic_' + str(frame_dict['rotation']) 
			cv2.imwrite(filename + '.png', img)
			print('Frame selected')
			pcl.save(ptcloud, path=filename +'.pcd')


def capture_point_cloud(filename='test'):
	global _frame_cloud
	#visual = pcl.pcl_visualization.CloudViewing()
	#visual.ShowMonochromeCloud(frame_cloud, b'cloud')
	if _frame_cloud is not None:
		print('saving pcd file: '+ filename)
		pcl.save(ptcloud, path=filename +'.pcd')
			

if __name__ == '__main__':
    #show_webcam()
    start_lidar_pipeline()
