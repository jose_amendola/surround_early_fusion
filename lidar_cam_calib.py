import os 
os.environ["OPENBLAS_CORETYPE"] = "nehalem"
import cv2
import numpy as np
import json
from os1 import OS1
from multiprocessing import Process, Queue
from os1.packet import (
	AZIMUTH_BLOCK_COUNT,
	CHANNEL_BLOCK_COUNT,
	azimuth_angle,
	azimuth_block,
	azimuth_measurement_id,
	azimuth_timestamp,
	azimuth_valid,
	channel_block,
	channel_range,
	unpack,
	PACKET
)
from os1.utils import xyz_points, build_trig_table
import pcl
import pcl.pcl_visualization
from time import sleep
import math
import select

# The OS-16 will still contain 64 channels in the packet, but only
# every 4th channel starting at the 2nd will contain data .
OS_16_CHANNELS = (2, 6, 10, 14, 18, 22, 26, 30, 34, 38, 42, 46, 50, 54, 58, 62)
OS_64_CHANNELS = tuple(i for i in range(CHANNEL_BLOCK_COUNT))


unprocessed_packets = Queue()
extracted_coords = Queue()
point_cloud = None
visual = pcl.pcl_visualization.CloudViewing()
#visual_plane = pcl.pcl_visualization.CloudViewing()
_display = True
coords = np.empty(shape=(0,3),dtype=np.float32)
_mem_frame = None

OS1_IP = '169.254.123.132'
HOST_IP = '169.254.17.164'

def handler(packet):
    unprocessed_packets.put(packet)

def spawn_workers(n, worker, *args, **kwargs):
    processes = []
    for i in range(n):
        process = Process(
            target=worker,
            args=args,
            kwargs=kwargs
        )
        process.start()
        processes.append(process)
    return processes

def build_trig_table(beam_altitude_angles, beam_azimuth_angles):
	table = []
	for i in range(CHANNEL_BLOCK_COUNT):
		table.append([
		    math.sin(beam_altitude_angles[i] * math.radians(1)),
		    math.cos(beam_altitude_angles[i] * math.radians(1)),
		    beam_azimuth_angles[i] * math.radians(1),
		])
	return table

def worker(lidar_ip, host_ip, out_queue) :
	os1 = OS1(lidar_ip, host_ip, mode='1024x10')	
	beam_intrinsics = json.loads(os1.get_beam_intrinsics())
	beam_alt_angles = beam_intrinsics['beam_altitude_angles']
	beam_az_angles = beam_intrinsics['beam_azimuth_angles']
	_trig_table = build_trig_table(beam_alt_angles, beam_az_angles)
	def handler(packet):
		nonlocal out_queue, _trig_table
		print('beam_altitude_angles: ', beam_alt_angles)
		print('beam_azimuth_angles: ', beam_az_angles)
		coords = np.empty(shape=(0,3),dtype=np.float32)
		x,y,z = filtered_xyz_points(packet, [60.0,120.0], _trig_table) 
		new_coords = np.array(list(zip(x,y,z)), dtype=np.float32)
		out_queue.put(new_coords)
	os1.start()
	try:
	    os1.run_forever(handler)
	except KeyboardInterrupt:
		for w in workers:
			w.terminate()

def xyz_point(channel_n, azimuth_block, trig_table):
	channel = channel_block(channel_n, azimuth_block)
	table_entry = trig_table[channel_n]
	range = channel_range(channel) / 1000  # to meters
	adjusted_angle = table_entry[2] + azimuth_angle(azimuth_block)
	x = -range * table_entry[1] * math.cos(adjusted_angle)
	y = range * table_entry[1] * math.sin(adjusted_angle)
	z = range * table_entry[0]

	return [x, y, z], adjusted_angle


def filtered_xyz_points(packet, angle_interval, trig_table,  os16=False):
	lower = angle_interval[0]*(np.pi/180)
	upper = angle_interval[1]*(np.pi/180)
	
	print('lower: ', lower)
	print('upper: ', upper)
	channels = OS_16_CHANNELS if os16 else OS_64_CHANNELS
	if not isinstance(packet, tuple):
		packet = unpack(packet)

	x = []
	y = []
	z = []

	for b in range(AZIMUTH_BLOCK_COUNT):
		block = azimuth_block(b, packet)

		if not azimuth_valid(block):
		    	continue

		for c in channels:
			point, angle = xyz_point(c, block, trig_table)
			if lower < upper:
				if angle > lower and angle < upper:
					x.append(point[0])
					y.append(point[1])
					z.append(point[2])
			else:
				if angle > lower or angle < upper:
					x.append(point[0])
					y.append(point[1])
					z.append(point[2])
			
	return x, y, z


def test(raw_packet):
	global coords, _mem_frame
	cur_frame = _mem_frame
	if not isinstance(raw_packet, tuple):
		raw_packet = unpack(raw_packet)
	for b in range(AZIMUTH_BLOCK_COUNT):
		block = azimuth_block(b, raw_packet)
		cur_frame = azimuth_frame_id(block)
		if _mem_frame is None:
			_mem_frame = cur_frame		
	print(cur_frame)
	print(_mem_frame)
	if cur_frame != _mem_frame:
		print('Finished surround scan')
		_mem_frame = cur_frame
		ptcloud = pcl.PointCloud()
		ptcloud.from_array(coords)
		print('acquiring point clouds')
		display_pointcloud(ptcloud)
		coords = np.empty(shape=(0,3),dtype=np.float32)
	x, y, z = xyz_points(raw_packet)
	frame = np.array(list(zip(x,y,z)), dtype=np.float32)
	coords = np.concatenate((coords, frame), axis=0)
	#extract_planes(ptcloud)
		

def prefilter_pointcloud(points):
	pass

	
		

def display_pointcloud(ptcloud):
	global _display
	if _display == True and _searching_plane == True:
		visual.ShowMonochromeCloud(ptcloud, b'cloud')
		_display = not(visual.WasStopped())
		

def extract_planes(ptcloud):
	global _searching_plane
	seg = ptcloud.make_segmenter_normals(ksearch=50)
	seg.set_optimize_coefficients(True)
	seg.set_model_type(pcl.SACMODEL_NORMAL_PLANE)
	seg.set_method_type(pcl.SAC_RANSAC)
	seg.set_distance_threshold(0.01)
	seg.set_normal_distance_weight(0.01)
	seg.set_max_iterations(100)
	indices, coefficients = seg.segment()
	if len(indices) == 0:
        	print('Could not estimate a planar model for the given dataset.')
	else:
		print('Found plane!')
		#_searching_plane = False
		#plane_cloud = ptcloud.extract(indices)
		#visual.ShowMonochromeCloud(ptcloud, b'plane')

	



def initialize_lidar():
	workers = spawn_workers(1, worker, OS1_IP, HOST_IP, extracted_coords)

	



def find_blobs():
	pass



def main():
	initialize_lidar()
	coords = np.empty(shape=(0,3),dtype=np.float32)
	scanned = False
	sec = None
	for i in range(500):
		#sleep(0.1)
		#print('not blocked')
		#print('Size of queue ', extracted_coords.qsize())
		frame = extracted_coords.get()
		if len(frame) > 0:
			scanned = False
			coords = np.concatenate((coords, frame), axis=0)
			ptcloud = pcl.PointCloud()
			ptcloud.from_array(coords)
			visual.ShowMonochromeCloud(ptcloud, b'cloud')
	ptcloud = pcl.PointCloud()
	ptcloud.from_array(coords)
	#np.save(coords, 'demo.npy')
	print('saving file')
	pcl.save(ptcloud, path='./demo.pcd')
			

if __name__ == '__main__':
    #show_webcam()
    main()
