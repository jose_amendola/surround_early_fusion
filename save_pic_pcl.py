import os 
os.environ["OPENBLAS_CORETYPE"] = "nehalem"

import cv2
import numpy as np
import json
from lidar_to_file import capture_point_cloud


#North, south, west, east
#cam_devices = ['/dev/video0','/dev/video1','/dev/video2','/dev/video3']
cam_devices = ['/dev/video0']
n_frames = 10
# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 50, 0.001)


def show_webcam(mirror=False):
    cam = cv2.VideoCapture(0)
    while True:
        ret_val, img = cam.read()
        if mirror: 
            img = cv2.flip(img, 1)
        cv2.imshow('my webcam', img)
        if cv2.waitKey(1) == 27: 
            break  # esc to quit
    cv2.destroyAllWindows()

def array_to_list(array):
    if isinstance(array, np.ndarray):
        return array_to_list(array.tolist())
    elif isinstance(array, list):
        return [array_to_list(item) for item in array]
    elif isinstance(array, tuple):
        return tuple(array_to_list(item) for item in array)
    else:
        return array

def acquire_frame(cam_id):
	cam_index = int(cam_id[-1])
	cam = cv2.VideoCapture(cam_index)
	ret_val, img = cam.read()
	return img



def save_frames(cam_id, n_images):
	# Loop over the image files
	cam_index = int(cam_id[-1])
	cam = cv2.VideoCapture(cam_index)
	for i in range(n_images):
		while True:
			# Load the image and convert it to gray scale
			ret_val, img = cam.read(cv2.IMREAD_GRAYSCALE)
			# Set up the blob detector.
			detector = cv2.SimpleBlobDetector_create()

			# Detect blobs from the image.
			keypoints = detector.detect(img)

			# Draw detected blobs as red circles.
			# cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS - This method draws detected blobs as red circles and ensures that the size of the circle corresponds to the size of the blob.
			blobs = cv2.drawKeypoints(img, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
			# Display the image
			cv2.imshow('blobs', blobs)
			if cv2.waitKey(1) == 32:
				cv2.waitKey(0)
				filename ='pic_' + cam_id[-1] + '_'+str(i) 
				cv2.imwrite(filename + '.png', img)
				capture_point_cloud(filename)
				print('Frame selected')
				break	  
		cv2.destroyAllWindows()


def main():
	for cam_id in cam_devices:
		obj_pts, img_pts, gray = save_frames(cam_id, n_frames)

if __name__ == '__main__':
    #show_webcam()
    main()
