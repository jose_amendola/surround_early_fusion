import os 
os.environ["OPENBLAS_CORETYPE"] = "nehalem"
import cv2
import numpy as np
import json
import pcl
import pcl.pcl_visualization
import pcl
#visual = pcl.pcl_visualization.CloudViewing()
visual = pcl.pcl_visualization.PCLVisualizering()


def find_calib_plane(ptcloud):
	clipper = ptcloud.make_cropbox()
	outcloud = pcl.PointCloud()
	clipper.set_Translation(0, 0, 0)
	clipper.set_Rotation(0, 0, 0)
	minx = -1
	miny = 0
	minz = -2
	mins = 1
	maxx = 1
	maxy = 1.4
	maxz = 3
	maxs = 1
	clipper.set_MinMax(minx, miny, minz, mins, maxx, maxy, maxz, maxs)
	clipped_cloud = clipper.filter()

	seg = clipped_cloud.make_segmenter_normals(ksearch=50)
	seg.set_optimize_coefficients(True)
	seg.set_model_type(pcl.SACMODEL_NORMAL_PLANE)
	seg.set_axis(0,1,0)
	seg.set_min_max_opening_angle(1,1)
	seg.set_method_type(pcl.SAC_RANSAC)
	seg.set_distance_threshold(0.02)
	seg.set_normal_distance_weight(0.01)
	seg.set_max_iterations(100)
	indices, coefficients = seg.segment()

	plane_cloud = clipped_cloud.extract(indices, negative=False)
	return plane_cloud, coefficients
	
	#fil = plane_cloud.make_passthrough_filter()
	#fil.set_filter_field_name("z")
	#fil.set_filter_limits(0, 1.5)
	#cloud_filtered = fil.filter()

def get_normals(ptcloud):
	normal_est = ptcloud.make_NormalEstimation()
	normal_est.set_KSearch(10)
	normals = normal_est.compute()
	return normals

def project_on_plane(ptcloud, coefficients):
	normal = np.array(coefficients[:-1])
	cloud_array = np.asarray(ptcloud)
	projected_cloud_list = []
	for point in cloud_array:
		projected_cloud_list.append(point - (normal*(np.dot(point,normal) + coefficients[-1])))
	projected_cloud_array = np.array(projected_cloud_list, dtype=np.float32)
	return pcl.PointCloud(projected_cloud_array)

def extract_edges(ptcloud):
	threshold = 0.4
	cloud_array = np.asarray(ptcloud)
	extracted_edges = []
	for i in range(len(cloud_array) -1):
		if np.linalg.norm(cloud_array[i+1]-cloud_array[i]) > threshold:
			extracted_edges.append(cloud_array[i])
			extracted_edges.append(cloud_array[i+1])
	return pcl.PointCloud(extracted_edges)

def extract_circles(ptcloud, normal_axis):
	edge_list = list()
	for i in range(4):
		seg = ptcloud.make_segmenter_normals(ksearch=50)
		seg.set_optimize_coefficients(True)
		seg.set_model_type(pcl.SACMODEL_CIRCLE3D)
		seg.set_axis(normal_axis[0],normal_axis[1],normal_axis[2])
		seg.set_radius_limits(0.1, 0.2)
		seg.set_min_max_opening_angle(1,1)
		seg.set_method_type(pcl.SAC_RANSAC)
		seg.set_distance_threshold(0.02)
		seg.set_normal_distance_weight(0.01)
		seg.set_max_iterations(100)
		indices, coefficients = seg.segment()
		partial = ptcloud.extract(indices, negative=False).to_list()
		edge_list = edge_list + partial
	return pcl.PointCloud(edge_list), indices
	
	
	


if __name__ == '__main__':
    #show_webcam()
	p = pcl.load("demo.pcd")
	cloud_plane, coefficients = find_calib_plane(p)
	cloud_projected = project_on_plane(cloud_plane, coefficients)

	edges, coe = extract_circles(cloud_projected, coefficients[:-1])
	print(coe)
	normals = get_normals(edges)
	visual.AddPointCloud(edges,b'cloud')
	#visual.AddPointCloudNormals(edges, normals, level=1)
	visual.AddCoordinateSystem()
	visual.Spin()
	#v = True
	#while v:
		#v = not(visual.WasStopped())
