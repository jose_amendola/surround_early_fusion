import os
os.environ["OPENBLAS_CORETYPE"] = "nehalem"
import json
import cv2
import numpy as np
import json
from pcd_reader import BlobExtractor3D

#North, south, west, east
#cam_devices = ['/dev/video0','/dev/video1','/dev/video2','/dev/video3']
cam_devices = ['/dev/video0']
n_frames = 10
# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 50, 0.001)

# Define blobs
objp = np.zeros((4, 3), np.float32)
objp[0]  = (-0.278, 0.149, 0)
objp[1]  = (0.278, 0.149, 0)
objp[2]  = (-0.278, -0.149, 0)
objp[3]  = (0.278, -0.149, 0)




def array_to_list(array):
    if isinstance(array, np.ndarray):
        return array_to_list(array.tolist())
    elif isinstance(array, list):
        return [array_to_list(item) for item in array]
    elif isinstance(array, tuple):
        return tuple(array_to_list(item) for item in array)
    else:
        return array


def hough_circles(im):
	gray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
	rows = gray.shape[0]
	circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1, rows / 8, param1=100, param2=30, minRadius=20, maxRadius=30)
	if circles is not None:
		circles = np.uint16(np.around(circles))
		centers = np.zeros(shape=(0,2))

		for i in circles[0,:]:
			center = (i[0], i[1])
			centers = np.vstack((centers, center))
			# circle center
			cv2.circle(im, center, 1, (0, 0, 100), 3)
			# circle outline
			radius = i[2]
			cv2.circle(im, center, radius, (255, 0, 0), 3)
	cv2.imshow("Hough circles", im)
	cv2.waitKey(0)
	return centers

def extract_pointcloud_centers():
	pass

def reorder_points_from_img(pts):
	#top left-top right-bottomleft-bottom right
	center_pt = np.mean(pts, axis=0)
	top_left = np.squeeze(pts[(pts[:,0] < center_pt[0]) & (pts[:,1] < center_pt[1])])
	#ordered_pts.append(top_left)
	top_right = np.squeeze(pts[(pts[:,0] > center_pt[0]) & (pts[:,1] < center_pt[1])])
	#ordered_pts.append(top_right)
	bottom_left = np.squeeze(pts[(pts[:,0] < center_pt[0]) & (pts[:,1] > center_pt[1])])
	#ordered_pts.append(bottom_left)
	bottom_right = np.squeeze(pts[(pts[:, 0] > center_pt[0]) & (pts[:, 1] > center_pt[1])])
	#ordered_pts.append(bottom_right)
	return np.vstack([top_left, top_right, bottom_left, bottom_right])


def extract_3d_centers(folder, pcd_file):
	extr = BlobExtractor3D(folder, pcd_file)
	extr.crop((-0.5, 0.0001, -0.6), (0.4, 2.0, 0.15))
	extr.set_lines()
	extr.organize_point_cloud()
	extr.project_on_plane()
	extr.find_edges()
	extr.separate_clusters()
	pts = extr.get_ordered_centers()
	return pts


def rigid_transform_3D(A, B):
    assert A.shape == B.shape

    num_rows, num_cols = A.shape
    if num_rows != 3:
        raise Exception(f"matrix A is not 3xN, it is {num_rows}x{num_cols}")

    num_rows, num_cols = B.shape
    if num_rows != 3:
        raise Exception(f"matrix B is not 3xN, it is {num_rows}x{num_cols}")

    # find mean column wise
    centroid_A = np.mean(A, axis=1)
    centroid_B = np.mean(B, axis=1)

    # ensure centroids are 3x1
    centroid_A = centroid_A.reshape(-1, 1)
    centroid_B = centroid_B.reshape(-1, 1)

    # subtract mean
    Am = A - centroid_A
    Bm = B - centroid_B

    H = Am @ np.transpose(Bm)

    # sanity check
    #if linalg.matrix_rank(H) < 3:
    #    raise ValueError("rank of H = {}, expecting 3".format(linalg.matrix_rank(H)))

    # find rotation
    U, S, Vt = np.linalg.svd(H)
    R = Vt.T @ U.T

    # special reflection case
    if np.linalg.det(R) < 0:
        print("det(R) < R, reflection detected!, correcting for it ...")
        Vt[2,:] *= -1
        R = Vt.T @ U.T

    t = -R @ centroid_A + centroid_B

    return R, t

def main():
	# Read image
	folder = './frames_floor/'
	filename = "pic_1722.png"
	pcd_file = 'pic_1722.pcd'
	# ordered_pcd_centers = extract_3d_centers(folder, pcd_file)
	extr = BlobExtractor3D(folder, pcd_file)
	extr.crop((-0.5, 0.0001, -0.6), (0.4, 2.0, 0.15))
	extr.set_lines()
	extr.organize_point_cloud()
	extr.project_on_plane()
	extr.find_edges()
	extr.separate_clusters()
	pcd_pts = extr.get_ordered_centers()
	intrinsic_file = 'calibration_cam_0.json'
	im = cv2.imread(folder+filename)
	centers = hough_circles(im)
	print(centers)
	ordered_im_centers = reorder_points_from_img(centers)
	print(ordered_im_centers)
	with open(folder+intrinsic_file, 'r') as intr_file:
		intrinsic = json.load(intr_file)
	mtx = np.asarray(intrinsic['mtx'])
	dist = np.asarray(intrinsic['dist'])
	ret,rvecs, tvecs = cv2.solvePnP(objp, ordered_im_centers, mtx, dist)
	# project 3D points to image plane
	imgpts, jac  = cv2.projectPoints(objp, rvecs, tvecs, mtx, dist)
	# for pt in imgpts:
	# 	aux = np.squeeze(pt)
	# 	cv2.circle(im, (int(aux[0]), int(aux[1])), 1, (0, 200, 0), 3)
	# cv2.imshow('img', im)
	# cv2.waitKey(0)

	R_o_wrt_lidar, t_o_wrt_lidar = rigid_transform_3D(objp.T, pcd_pts.T)
	print(R_o_wrt_lidar, t_o_wrt_lidar)

	reproj_pts_t = np.linalg.inv(R_o_wrt_lidar)@pcd_pts.T - np.linalg.inv(R_o_wrt_lidar)@t_o_wrt_lidar

	test_pts, jac = cv2.projectPoints(reproj_pts_t.T, rvecs, tvecs, mtx, dist)
	for pt in test_pts:
		aux = np.squeeze(pt)
		cv2.circle(im, (int(aux[0]), int(aux[1])), 1, (200, 0, 0), 3)
	cv2.imshow('img', im)
	cv2.waitKey(0)

	extr.project_cylinder_meshes()
	extr.transform_meshes(R_o_wrt_lidar, t_o_wrt_lidar)
	extr.view()





if __name__ == '__main__':
    main()
