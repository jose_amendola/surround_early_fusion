import os 
os.environ["OPENBLAS_CORETYPE"] = "nehalem"

import cv2
import numpy as np
import json


#North, south, west, east
cam_devices = ['/dev/video0','/dev/video1','/dev/video2','/dev/video3']
n_frames = 20
# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 50, 0.001)

# Define the chess board rows and columns. It should be the number of inside corners in the chessboard
rows = 9
cols = 7

# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objectPoints = np.zeros((rows * cols, 3), np.float32)
objectPoints[:, :2] = np.mgrid[0:rows, 0:cols].T.reshape(-1, 2)*2
objectPoints.shape


def show_webcam(mirror=False):
    cam = cv2.VideoCapture(0)
    while True:
        ret_val, img = cam.read()
        if mirror: 
            img = cv2.flip(img, 1)
        cv2.imshow('my webcam', img)
        if cv2.waitKey(1) == 27: 
            break  # esc to quit
    cv2.destroyAllWindows()

def array_to_list(array):
    if isinstance(array, np.ndarray):
        return array_to_list(array.tolist())
    elif isinstance(array, list):
        return [array_to_list(item) for item in array]
    elif isinstance(array, tuple):
        return tuple(array_to_list(item) for item in array)
    else:
        return array

def acquire_frame(cam_id):
	cam_index = int(cam_id[-1])
	cam = cv2.VideoCapture(cam_index)
	ret_val, img = cam.read()
	return img


def calibrate_and_save(file_name, obj_points_array, img_point_array, gray):
	ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(obj_points_array, img_point_array,
		gray.shape[::-1], None, None)
	output = dict()
	output['mtx'] = array_to_list(mtx)
	output['dist'] = array_to_list(dist)
	output['rvecs'] = array_to_list(rvecs)
	output['tvecs'] = array_to_list(tvecs)
	with open(file_name+'.json', 'w') as fp:
	    json.dump(output, fp)
	print(output)
	return ret, mtx, dist, rvecs, tvecs 

def display_corrected_img(img, mtx, dist):
	h,  w = img.shape[:2]
	newcameramtx, roi = cv2.getOptimalNewCameraMatrix(mtx, dist, (w,h), 1, (w,h))
	dst = cv2.undistort(img, mtx, dist, None, newcameramtx)
	# crop the image
	x, y, w, h = roi
	#dst = dst[y:y+h, x:x+w]
	cv2.imshow('Undistorted image', dst)
	cv2.waitKey(0)

def extract_points(cam_id, n_images):
	# Arrays to store object points and image points from all the images.
	objectPointsArray = [] # 3d point in real world space
	imgPointsArray = [] # 2d points in image plane.
	# Loop over the image files
	cam_index = int(cam_id[-1])
	cam = cv2.VideoCapture(cam_index)
	for i in range(n_images):
		ret = False
		while True:
			# Load the image and convert it to gray scale
			ret_val, img = cam.read(cv2.IMREAD_GRAYSCALE)
			gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
			print('Shape of image ', gray.shape)
			# Find the chess board corners
			ret, corners = cv2.findChessboardCorners(gray, (rows, cols), None)		
			# Make sure the chess board pattern was found in the image
			if ret:
				# Refine the corner position
		    		corners = cv2.cornerSubPix(gray, corners, (rows, cols), (-1, -1), criteria)
				# Draw the corners on the image
	    			cv2.drawChessboardCorners(img, (rows, cols), corners, ret)
			# Display the image
			cv2.imshow('chess board', img)
			if cv2.waitKey(1) == 32:
				if ret:
					cv2.waitKey(0)
					print('Frame selected')
					break	
		    
		    
		# Add the object points and the image points to the arrays
		objectPointsArray.append(objectPoints)
		imgPointsArray.append(corners)	    
		   
		cv2.destroyAllWindows()
	return objectPointsArray, imgPointsArray, gray

def main():
	for cam_id in cam_devices:
		obj_pts, img_pts, gray = extract_points(cam_id, n_frames)
		ret, mtx, dist, rvecs, tvecs = calibrate_and_save('calibration_cam_'+cam_id[-1], obj_pts, img_pts, gray)
		display_corrected_img(gray, mtx, dist)
		

if __name__ == '__main__':
    #show_webcam()
    main()
