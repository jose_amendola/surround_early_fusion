import open3d as o3d
import numpy as np
from ai import cs
import pyransac3d as pyrsc
import itertools




_folder = './calib_frames/'

class BlobExtractor3D(object):
    def __init__(self, fldr, flnm):
        self.folder = fldr
        self.filename = flnm
        self.ptcloud = o3d.io.read_point_cloud(self.folder+self.filename)
        self.viewer = o3d.visualization.Visualizer()
        self.plane_model = None
        self.inliers = None
        self.support_ptcloud = o3d.geometry.PointCloud()
        self.line_set = o3d.geometry.LineSet()

        self.origin_sphere = o3d.geometry.TriangleMesh.create_sphere(radius=0.1)
        self.circle_centers = None
        self.other_meshes = list()

    def crop(self, point_a, point_b):
        bbox = o3d.geometry.AxisAlignedBoundingBox.create_from_points(
        o3d.utility.Vector3dVector([point_a, point_b]))
        self.ptcloud = self.ptcloud.crop(bbox)

    def view(self):
        self.viewer.create_window()
        # self.viewer.add_geometry(self.ptcloud)
        self.viewer.add_geometry(self.support_ptcloud)
        # self.viewer.add_geometry(self.line_set)
        # self.viewer.add_geometry(self.origin_sphere)
        for mesh in self.other_meshes:
            self.viewer.add_geometry(mesh)
        opt = self.viewer.get_render_option()
        c_frame = o3d.geometry.TriangleMesh.create_coordinate_frame()
        self.viewer.add_geometry(c_frame)
        opt.background_color = np.asarray([0.5, 0.5, 0.5])
        self.viewer.run()

    def project_on_plane(self):
        self.plane_model, self.inliers = self.ptcloud.segment_plane(distance_threshold=0.04,
                                                 ransac_n=100,
                                                 num_iterations=1000)
        [A, B, C, D] = self.plane_model
        x0, y0, z0 = 0, 0, 0
        x1 = np.asarray(self.ptcloud.points)[self.inliers, 0]
        y1 = np.asarray(self.ptcloud.points)[self.inliers, 1]
        z1 = np.asarray(self.ptcloud.points)[self.inliers, 2]
        x0 = x0 * np.ones(x1.size)
        y0 = y0 * np.ones(y1.size)
        z0 = z0 * np.ones(z1.size)
        r = np.power(np.square(x1 - x0) + np.square(y1 - y0) + np.square(z1 - z0), 0.5)
        a = (x1 - x0) / r
        b = (y1 - y0) / r
        c = (z1 - z0) / r
        t = -1 * (A * np.asarray(self.ptcloud.points)[self.inliers, 0] + B * np.asarray(self.ptcloud.points)[self.inliers, 1] + C *
                  np.asarray(self.ptcloud.points)[self.inliers, 2] + D)
        t = t / (a * A + b * B + c * C)
        np.asarray(self.ptcloud.points)[self.inliers, 0] = x1 + a * t
        np.asarray(self.ptcloud.points)[self.inliers, 1] = y1 + b * t
        np.asarray(self.ptcloud.points)[self.inliers, 2] = z1 + c * t


    def convert_to_cylinder(self, pts):
        r, phi, z = cs.cart2cyl(x=pts[:, 0], y=pts[:, 1], z=pts[:, 2])
        return r, phi, z

    def organize_point_cloud(self):
        pts = np.asarray(self.ptcloud.points)
        new_pts = np.empty(shape=(0,4))
        r, phi, z = self.convert_to_cylinder(pts)
        cyl_pts = np.column_stack((list(range(len(r))), r, phi, z))
        cyl_pts = cyl_pts[cyl_pts[:, 3].argsort(kind='mergesort')]  # sort by z
        bins = np.arange(-0.2, 0.5, 0.01)
        binned_indices = np.digitize(cyl_pts[:, 3], bins=bins)
        for i, bin_val in enumerate(bins):
            bool_mask = binned_indices == i
            bin_pts = cyl_pts[bool_mask]
            bin_pts = bin_pts[bin_pts[:, 2].argsort(kind='mergesort')]
            new_pts = np.concatenate((new_pts, bin_pts))
        self.ptcloud.points = o3d.utility.Vector3dVector(pts[new_pts[:,0].astype(int)])

    def find_edges(self):
        plane_normal = self.plane_model[:-1]
        pts = np.asarray(self.ptcloud.points)
        borders = list()
        def sig_dist(point):
            [x1,y1,z1] = point
            [a, b, c, d] = self.plane_model
            return (a*x1 + b*y1 + c*z1 + d) / np.sqrt(a**2 + b**2 + c**2)
        outlier_indices = [i for i,pt in enumerate(pts) if i not in self.inliers and sig_dist(pt) > 0.05]
        # outlier_pts = [pt for i, pt in enumerate(pts) if i not in self.inliers and sig_dist(pt) > 0.1]
        for i, pt in enumerate(pts[1:-1]):
            if i in self.inliers:
                pos_inlier = self.inliers.index(i)
                check_prev_out = i-1 in outlier_indices
                check_next_out = i + 1 in outlier_indices
                prev_inlier_dist = 1000
                if pos_inlier > 0:
                    prev_inlier_dist = np.linalg.norm(pts[i]-pts[self.inliers[pos_inlier-1]])
                next_inlier_dist = 1000
                if pos_inlier < len(self.inliers)-1:
                    next_inlier_dist = np.linalg.norm(pts[i]-pts[self.inliers[pos_inlier+1]])
                min_dist = 0.08
                max_dist = 0.5
                if check_prev_out or check_next_out:
                    if min_dist < prev_inlier_dist < max_dist or min_dist < next_inlier_dist < max_dist:
                        borders.append(pts[i])
        self.ptcloud.points = o3d.utility.Vector3dVector(borders)

    def set_lines(self):
        pts = np.asarray(self.ptcloud.points)
        lines = list()
        line_pts = list()
        init = 0
        for i, pt in enumerate(pts[:-1]):
            line_pts.append(list(pts[i]))
            line_pts.append(list(pts[i + 1]))
            lines.append([init, init + 1])
            init = init + 2

        self.line_set = o3d.geometry.LineSet()
        self.line_set.points = o3d.utility.Vector3dVector(line_pts)
        self.line_set.lines = o3d.utility.Vector2iVector(lines)

    def separate_clusters(self):
        import matplotlib.pyplot as plt
        labels = None
        with o3d.utility.VerbosityContextManager(
                o3d.utility.VerbosityLevel.Debug) as cm:
            labels = np.array(
                self.ptcloud.cluster_dbscan(eps=0.05, min_points=5))

        outlier_mask = labels != -1
        pts = np.asarray(self.ptcloud.points)
        self.ptcloud.points = o3d.utility.Vector3dVector(pts[outlier_mask])

        with o3d.utility.VerbosityContextManager(
                o3d.utility.VerbosityLevel.Debug) as cm:
            labels = np.array(
                self.ptcloud.cluster_dbscan(eps=0.15, min_points=3))
        max_label = labels.max()
        print(f"point cloud has {max_label + 1} clusters")
        colors = plt.get_cmap("tab20")(labels / (max_label if max_label > 0 else 1))
        colors[labels < 0] = 0
        self.ptcloud.colors = o3d.utility.Vector3dVector(colors[:, :3])
        pts = np.asarray(self.ptcloud.points)
        self.circle_centers = np.zeros(shape=(0,3))
        for i in range(max_label+1):
            label_mask = labels == i
            cluster = pts[label_mask]
            circle = pyrsc.Circle()
            print('Try to adjust circle for cluster : ', i)
            center, radius, best_inliers = self.fit_3d_circle(cluster)
            # center, axis, radius, best_inliers = circle.fit(cluster, 0.1, maxIteration=10000)
            if center != []:
                self.circle_centers = np.vstack([self.circle_centers, center])
        self.support_ptcloud.points = o3d.utility.Vector3dVector(self.circle_centers)
        self.support_ptcloud.paint_uniform_color([0, 0, 0])

    def get_ordered_centers(self):
        # ordered_pts = list()
        r, phi, z = self.convert_to_cylinder(self.circle_centers)
        cyl_pts = np.vstack([r, phi, z])
        mean = np.mean(cyl_pts, axis=1)
        top_left = np.squeeze(self.circle_centers[(cyl_pts[1] > mean[1]) & (cyl_pts[2] > mean[2])])
        # ordered_pts.append(top_left)
        top_right = np.squeeze(self.circle_centers[(cyl_pts[1] < mean[1]) & (cyl_pts[2] > mean[2])])
        # ordered_pts.append(top_right)
        bottom_left = np.squeeze(self.circle_centers[(cyl_pts[1] > mean[1]) & (cyl_pts[2] < mean[2])])
        # ordered_pts.append(bottom_left)
        bottom_right = np.squeeze(self.circle_centers[(cyl_pts[1] < mean[1]) & (cyl_pts[2] < mean[2])])
        # ordered_pts.append(bottom_right)
        return np.vstack([top_left, top_right, bottom_left, bottom_right])

    def paint_pts(self, pt_list):
        pts_array = np.vstack(pt_list)
        colors = np.array([[0,0,0],[255,0,0],[0,255,0],[0,0,255]])
        # cloud = o3d.geometry.PointCloud()
        self.support_ptcloud.points = o3d.utility.Vector3dVector(pts_array)
        self.support_ptcloud.colors = o3d.utility.Vector3dVector(colors)
        # self.viewer.add_geometry(cloud)


    def remove_straight_borders(self):
        pts = np.asarray(self.ptcloud.points)
        line = pyrsc.Line()
        inliers_count = 1000
        filtered = pts
        # while inliers_count > 20:
        for i in range(1):
            A, B, best_inliers = line.fit(filtered, thresh=0.01)
            inliers_count = len(best_inliers)
            print(A, B, best_inliers)
            filtered = np.delete(filtered, best_inliers, axis=0)
        self.ptcloud.points = o3d.utility.Vector3dVector(filtered)

    def fit_circles(self):
        pts = np.asarray(self.ptcloud.points)
        # center, r_b, inliers = self.fit_3d_circle(pts)
        potentials = self.find_potentials(pts)
        self.support_ptcloud.points = o3d.utility.Vector3dVector(pts[potentials])
        # print(center, r_b, inliers)
        # print(center, axis, radius, best_inliers)
        self.support_ptcloud.paint_uniform_color([0, 0, 0])

    def fit_3d_circle(self, pts):
        # lists
        xList = pts[:, 0]
        yList = pts[:, 1]
        zList = pts[:, 2]
        # Model
        x = 0
        y = 0
        z = 0
        r = 0

        # n – minimum number of data points required to estimate model parameters
        # k – maximum number of iterations allowed in the algorithm
        # t – threshold value to determine data points that are fit well by model
        # d – number of close data points required to assert that a model fits well to data

        n = 5
        k = 1000
        t = 0.015
        d = 3

        iterations = 0

        # Updated model
        x_b = 0
        y_b = 0
        z_b = 0
        r_b = 0
        bestErr = 999
        updateErrCount = 1

        print("Estimation:")
        while iterations < k:
            iterations += 1
            maybeInliers = np.random.choice(len(xList), n, replace=False)
            alsoInliers = []

            x = np.mean(xList[maybeInliers])
            y = np.mean(yList[maybeInliers])
            z = np.mean(zList[maybeInliers])

            r_array = np.sqrt((xList[maybeInliers] - x) ** 2 + (yList[maybeInliers] - y) ** 2 + (zList[maybeInliers] - z) ** 2)
            r = np.mean(r_array)
            # if r < 0.13 or r > 0.15:
            #     print('radius out of range  ',r)
            #     continue

            other_indices = np.delete(np.arange(0,len(xList)), maybeInliers)

            dists = np.abs(np.sqrt((xList[other_indices]-x) ** 2 + (yList[other_indices]-y) ** 2 + (zList[other_indices]-z) ** 2) - r)

            inlier_mask = dists < t

            alsoInliers = other_indices[inlier_mask]
            # if alsoInliers are bigger than the closed data points required
            inliers = list()
            if len(alsoInliers) > d:

                inliers = np.concatenate((maybeInliers, alsoInliers), axis=0)

                # model based on inliers
                x = np.mean(xList[inliers])
                y = np.mean(yList[inliers])
                z = np.mean(zList[inliers])
                r = 0

                r_array = np.sqrt((xList[inliers] - x) ** 2 + (yList[inliers] - y) ** 2 + (zList[inliers] - z) ** 2)
                r = np.mean(r_array)

                # calculating lsm for the inliers
                lsm = np.sum(np.abs(np.sqrt((xList[other_indices] - x) ** 2 + (yList[other_indices] - y) ** 2 + (
                            zList[other_indices] - z) ** 2) - r))
                # if the calculated lsm is smaller than the previous error,
                # it updates the better model and bestErr value
                if lsm < bestErr:
                    x_b = x
                    y_b = y
                    z_b = z
                    r_b = r
                    bestErr = lsm
                    print("Number of updated errors: " + str(updateErrCount) + " - updated to: " + str(bestErr))
                    updateErrCount += 1

        # printout
        print(" ")
        print("Inliers: " + str(len(inliers)))
        print("Iterations: " + str(iterations))
        print(" ")
        print("Estimated model parameters:")
        print("Radius: " + str(r_b) + ", X: " + str(x_b) + ", Y: " + str(y_b))
        return [x_b, y_b, z_b], r_b, inliers

    def find_potentials(self, pts, radius=0.14, thr=0.005):
        def find_radius(p1, p2, p3):
            c = np.linalg.norm(p1-p2) ** 2
            a = np.linalg.norm(p2-p3) ** 2
            b = np.linalg.norm(p3 - p1) ** 2
            ar = a ** 0.5
            br = b ** 0.5
            cr = c ** 0.5
            r = ar * br * cr / ((ar + br + cr) * (-ar + br + cr) * (ar - br + cr) * (ar + br - cr)) ** 0.5
            return r

        combs = itertools.combinations(range(len(pts)), 3)
        potentials = list()
        # print('eval of combs ', len(list(combs)))
        for comb in combs:
            r = find_radius(pts[comb[0]], pts[comb[1]], pts[comb[2]])
            if np.abs(r - radius) < thr:
                potentials.append(comb[0])
                potentials.append(comb[1])
                potentials.append(comb[2])
        print('select potentials, ', len(potentials))
        (unique, counts) = np.unique(potentials, return_counts=True)
        print(unique, counts)
        return potentials

    def project_cylinder_meshes(self):
        objp = np.zeros((4, 3), np.float32)
        objp[0] = (-0.278, 0.149, 0)
        objp[1] = (0.278, 0.149, 0)
        objp[2] = (-0.278, -0.149, 0)
        objp[3] = (0.278, -0.149, 0)
        for i in objp[:]:
            cyl = o3d.geometry.TriangleMesh.create_cylinder(radius=0.07, height=10)
            cyl.translate(i)
            self.other_meshes.append(cyl)

    def transform_meshes(self, R, t):
        for mesh in self.other_meshes:
            mesh.rotate(R, center=mesh.get_center())
            mesh.translate(t)





    def __del__(self):
        self.viewer.destroy_window()


if __name__ == '__main__':
    folder = './frames_floor/'
    filename = 'pic_1722.pcd'
    extr = BlobExtractor3D(folder, filename)
    extr.crop((-0.5, 0.0001, -0.6), (0.4, 2.0, 0.15))
    extr.set_lines()
    extr.organize_point_cloud()
    extr.project_on_plane()
    extr.find_edges()
    extr.separate_clusters()
    pts = extr.get_ordered_centers()
    extr.paint_pts(pts)
    extr.project_cylinder_meshes()
    extr.view()